import sys

from django.conf import settings
from django.core.management import call_command

APP = 'contact'

if not settings.configured:
    settings.configure(
        ROOT_URLCONF='',
        DEBUG=False,
        # more conf if needed...
        DATABASES={
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': 'test.db'
            }
        },
        INSTALLED_APPS=(
            'south',
            APP,
        ),
    )

if __name__ == "__main__":
    is_initial = len(sys.argv) > 1 and sys.argv[0] == 'init'
    is_auto = not is_initial
    call_command('schemamigration', APP,
                 initial=is_initial,
                 auto=is_auto)
